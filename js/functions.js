 $(document).ready(function () {
               $('a#loader').on('click', function (e) {
                   e.preventDefault();
                    $('.loadmore-box').addClass('opened');
                   
                   // We're simulating loading something from elswhere here.
                   setTimeout(function() {
					   $('#loadmore-data').addClass('opened');
					   $('#loadmore-data span').html( $(this).attr('href'));                  
                   }.bind($(this)),500);
                   
                   return false;
               });
			   $('span.close').on('click', function (e) {
                  $(this).parent().removeClass('opened');
                  $(this).next().removeClass('opened');
               });               
});

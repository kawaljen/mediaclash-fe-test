# README #

Welcome to the Mediaclash Frontend Developer Thing!

It's a really small, rubbish, project with a couple of bugs we'd like you to have a go at. We'd like to see how you organize your commits and explain what each commit does as much as actually complete the test.

Please create a fork of this repository, commit your changes to that and submit a pull request back to me, thanks!

### Dependencies ###

To get started, firstly make sure you have [Vagrant](http://vagrantup.com) and [VirtualBox](https://www.virtualbox.org/) installed.

You'll also need a few GB of disk space to install the test virtual machine.

After cloning your fork of this repository, just navigate to the project folder and type:

```
#!bash

vagrant up
```

Ignore any warnings in the output, and when it completes the demo site should be running at:

[http://localhost:8080](http://localhost:8080)

When you are finished, you can delete the virtual machine with:

```
#!bash

vagrant destroy
```

### Tasks ###

1) The sub navigation ( the links starting with 'More info' ) are in the wrong place, they should be in a row below the main navigation, please fix.

2) There is a button on the page the should show a value after a short delay, it currently outputs 'undefined' but should alert something else, please fix.

3) The page is rather boring, can we put in any simple animations to make it better ? :)

Thanks very much for your time and Good Luck :)
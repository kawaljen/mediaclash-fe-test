module.exports = function (grunt) {
	grunt.initConfig({
		sass: {
			dist: {
				options: {
					style: 'expanded'
				},
				files: {
					'styles/css/main.css': 'styles/sass/main.scss'
				}
			}

		},
		
		watch : {
		    css: {
		        files: '**/*.scss',
		        tasks: ['sass'],
		        options: {
		            livereload: true
		        }
		    }
		}	
	});

	grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.registerTask('default', ['sass']);
};

